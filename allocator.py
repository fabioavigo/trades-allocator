"""
Small Python script for the allocation of trades among funds with a genetic algorithm.
Fábio Avigo - 2019/12/10
"""

import pandas as pd
import random
import copy
from datetime import datetime

parameters = dict()
parameters["alpha"] = 1
parameters["beta"] = 1
parameters["pool_size"] = 100
parameters["preserve_size"] = 50
parameters["mutation_chance"] = 0.01
parameters["iterations"] = 200
parameters["greedy_insertions"] = 10

class Trade():
    """Base class for a trade."""
    def __init__(self, amount, price):
        self.amount = amount
        self.price = price
        self.fund = None

class Asset():
    """Base class for the definition of an asset."""
    def __init__(self, name, type, min_amount):
        self.name = name
        self.type = type
        self.trades = []
        self.min_amount = min_amount
        self.average_price = 0.0
        self.total_position = 0.0
        self.funds = []

class Fund():
    """Base class for the definition of a fund."""
    def __init__(self, name, desired_percentage):
        self.name = name
        self.desired_percentage = desired_percentage
        self.desired_position = 0.0
        self.position = 0.0

class Solution():
    """Base class for a solution."""
    def __init__(self, asset, trades):
        self.asset = asset
        self.trades = trades
        
    def result(self):
        """Returns the resulting value of the solution."""
        alpha = parameters["alpha"]
        beta = parameters["beta"]
        result = 0.0
        for fund in self.asset.funds:
            trade_list = [t for t in self.trades.values() if t.fund == fund.name]
            avg_price = calculate_average_price(trade_list)
            total_position = calculate_total_position(trade_list)
            position_pc = (total_position * fund.desired_percentage) / fund.desired_position
            result += alpha * ((avg_price - self.asset.average_price) / self.asset.average_price) ** 2 + \
                      beta * ((position_pc - fund.desired_percentage) / fund.desired_percentage) **2
        return result

    def export_df(self):
        """Function to export its contents to a Pandas DataFrame."""
        data = []
        for trade in self.trades.values():
            data += [[self.asset.name, self.asset.type, trade.fund, trade.amount, trade.price]]
        df = pd.DataFrame(data=data, columns=['Asset', 'Type', 'Fund', 'Amount', 'Price']).groupby(['Asset', 'Type', 'Fund', 'Price']).sum().reset_index()
        return df

def calculate_average_price(trades):
    """Function that calculates the average price, given a list of trades."""
    amount = 0
    avg_price = 0.0
    for trade in trades:
        avg_price = ((avg_price * amount) + (trade.price * trade.amount)) / (amount + trade.amount)
        amount += trade.amount
    return avg_price

def calculate_total_position(trades):
    """Function that calculates the total position, given a list of trades."""
    pos = 0.0
    for trade in trades:
        pos += trade.amount * trade.price
    return pos

def combine_solutions(solution1, solution2):
    """Function that creates a child solution from two parent solutions."""
    mutation_chance = parameters["mutation_chance"]
    child_solution = copy.deepcopy(solution1)
    for key, trade in child_solution.trades.items():
        if random.random() < mutation_chance:
            trade.fund = random.choice([fund.name for fund in solution1.asset.funds])
        else:
            trade.fund = random.choice([solution1.trades[key].fund, solution2.trades[key].fund])

    return child_solution

def get_base_solution(asset):
    """Function that returns the base solution for a given asset."""
    solution = Solution(asset, dict())
    for i, trade in enumerate(asset.trades):
        solution.trades[i] = trade
    return solution

def get_random_solution(base_solution):
    """Function that generates a random solution from the base solution."""
    funds_list = [fund.name for fund in base_solution.asset.funds]
    solution = copy.deepcopy(base_solution)
    for trade in solution.trades.values():
        trade.fund=random.choice(funds_list)
    return solution

def get_greedy_solution(base_solution):
    """Function that generates a greedy solution from the base solution."""
    solution = copy.deepcopy(base_solution)
    funds_list = [fund.name for fund in base_solution.asset.funds]
    weights = [fund.desired_percentage for fund in base_solution.asset.funds]
    for trade in solution.trades.values():
        trade.fund=random.choices(population=funds_list, \
            weights=weights, k=1)[0]
    return solution

def initialize_assets():
    """Function for initializing the assets."""
    #Initialization
    df_trades = pd.read_excel("Trades.xlsx", sheet_name="Trades")
    for col in ["Asset", "Type"]:
        df_trades[col] = df_trades[col].apply(lambda x: str(x))
    df_trades = df_trades.groupby(["Asset", "Type", "Price"]).sum().reset_index()
    df_min_amount = pd.read_excel("Trades.xlsx", sheet_name="MinAmount")
    
    dict_min_amount = dict()
    for i in df_min_amount.index:
        dict_min_amount[df_min_amount.at[i, "Asset"]] = df_min_amount.at[i, "Value"]
    del df_min_amount

    assets = []
    for ticker in df_trades['Asset'].unique():
        if ticker in dict_min_amount.keys():
            min_amount = dict_min_amount[ticker]
        else:
            min_amount = 1
        
        df_filt = df_trades[df_trades['Asset']==ticker]
        for type in df_filt['Type'].unique():
            asset = Asset(ticker, type, min_amount)
            df_filt2 = df_filt[df_filt['Type']==type]
            for i in df_filt2.index:
                amount = df_filt2.at[i, 'Amount']
                price = df_filt2.at[i, 'Price']
                while amount >0:
                    if 2*asset.min_amount <= amount:
                        trade_amount = asset.min_amount
                    else:
                        trade_amount = amount
                    amount-= trade_amount
                    asset.trades.append(Trade(trade_amount, price))
                    
            assets.append(asset)
    
    #Average price per asset
    for asset in assets:
        asset.average_price = calculate_average_price(asset.trades)
        asset.total_position = calculate_total_position(asset.trades)
    return assets

def initialize_funds(assets):
    """Function for initializing funds."""
    df_funds = pd.read_excel("Trades.xlsx", sheet_name="Funds")
    for col in ["Fund", "Asset", "Type"]:
        df_funds[col] = df_funds[col].apply(lambda x: str(x))
        
    for fund_name in df_funds['Fund'].unique():
        df_filt = df_funds[df_funds['Fund']==fund_name]
        for ticker in df_filt['Asset'].unique():
            df_filt2 = df_filt[df_filt['Asset']==ticker]
            for type in df_filt2['Type'].unique():
                df_filt3 = df_filt2[df_filt2['Type']==type]
                try:
                    asset = next((x for x in assets if x.name == ticker and x.type == type), None)
                    fund = Fund(fund_name, df_filt3['% of Total'].sum())
                    fund.desired_position = fund.desired_percentage * asset.total_position
                    asset.funds.append(fund)
                
                except:
                    pass

def initialize_parameters():
    """Function for initializing paramenters."""
    df_param = pd.read_excel("Trades.xlsx", sheet_name="Parameters")
    for i in df_param.index:
        parameter = df_param.at[i, 'Parameter']
        if parameter in parameters.keys():
            parameters[parameter] = df_param.at[i, 'Value']
    
    for param in ["pool_size", "preserve_size", "iterations", "greedy_insertions"]:
        parameters[param] = int(parameters[param])

def iterate(solutions, base_solution):
    """Function that runs an iteration."""
    preserve_size = parameters["preserve_size"]
    pool_size = parameters["pool_size"]
    greedy_insertions = parameters["greedy_insertions"]
    preserved_solutions = sorted([sol for sol in solutions], key=lambda x: x.result())[0:preserve_size]
    pool = []
    while len(preserved_solutions) > 0:
        sol_1 = random.choice(preserved_solutions)
        preserved_solutions.remove(sol_1)
        sol_2 = random.choice(preserved_solutions)
        preserved_solutions.remove(sol_2)
        child_solution = combine_solutions(sol_1, sol_2)
        pool.append(sol_1)
        pool.append(sol_2)
        pool.append(child_solution)
    
    while len(pool) < pool_size:
        pool.append(get_random_solution(base_solution))    
    
    for i in range(greedy_insertions):
        pool.append(get_greedy_solution(base_solution))

    return pool

def start_solutions_pool(base_solution):
    """Function for starting the pool of solutions."""
    pool_size = parameters["pool_size"]
    greedy_insertions = parameters["greedy_insertions"]

    pool = []
    for i in range(pool_size):
        pool.append(get_random_solution(base_solution))

    for i in range(greedy_insertions):
        pool.append(get_greedy_solution(base_solution))

    return pool

def get_best_result(solutions):
    """Function that returns the best solution in a pool of solutions."""
    sol = sorted([sol for sol in solutions], key=lambda x: x.result())[0]
    return sol

def main():
    initialize_parameters()
    iterations = parameters["iterations"]
    assets = initialize_assets()
    initialize_funds(assets)
    df_result = pd.DataFrame()

    for asset in assets:
        base_solution = get_base_solution(asset)
        pool = start_solutions_pool(base_solution)
        sol = get_best_result(pool)
        print("Starting iterations... Best Starting Result: {}".format(sol.result()))
        for i in range(iterations):
            pool = iterate(pool, base_solution)
            sol = get_best_result(pool)
            print("Iteration {}. Best Result: {}".format(str(i), sol.result()))
        
        df_result = pd.concat([df_result, sol.export_df()])
    df_result.to_excel("Results_{}.xlsx".format(datetime.strftime(datetime.now(), "%Y%m%d%H%M%S")))
    print("Results exported.")

    
    
if __name__ == "__main__":
    main()