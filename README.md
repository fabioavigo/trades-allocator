**trades-allocator**

A small Python application for the allocation of trades among funds (portfolios)
with a genetic algorithm.

Quick how-to:
Download all the contents of the folder. Fill in the required data in the
"Trades.xlsx" worksheet. Run the program with python allocator.py. The
outputs will be on the file "Results.xlsx".

Required Inputs
To run the app, all needed data must be inserted in the "Trades.xlsx" file.

**Trades**: 
Insert all the trades to be allocated on this tab.

--Asset (string): name/ticker of the asset.

--Type (string): type of the operation. Any string is allowed. Common examples 
are 'B' or 'C' for buy operations and 'V' or 'S' for sell operations.

--Amount (integer): traded amount (>0).

--Price (float): trade price (>0).


**Funds**: Insert here the desired percentage for each asset on each fund.

--Fund (string): name of the fund.

--Asset (string): name/ticker of the asset (must have the exact name of 
Trades tab).

--% of Total (float): Percentage of total to be allocated in the fund 
(desired ratio). Must be a number between 0-1.


**Parameters**: Simulation parameters. Change as needed for calibration or
  faster runs.

--alpha (float): Multiplier for the squared normalized difference of the 
average price and the desired average price.

--beta (float): Multiplier for the squared normalized difference of the 
allocated percentage and the desired percentage.

--pool_size (integer): Number of solutions on each iteration run.

--preserve_size (integer): Number of preserved solutions (best) for each run. 
Preferrably an even number.
    
--mutation_chance (float): Probability that a gene of a child solution will 
mutate when generated. Must be a number between 0-1.

--iterations (integer): Total number of iterations.

--greedy_insertions (integer): Number of greedy solutions that are added on 
top of the pool size on each iteration, for reduced solution times.


**MinAmount**: Minimum traded amount for each asset. Default minimum is 1.

--Asset: name/ticker of the asset (same name of Trades tab).

--Value: Minimum amount for the asset (usually 1 or 100).

---------------------------

**Detailed Algorithm**

--Objective Function: The algorithm has the objective of minimizing the 
sum of two factors among all funds:
1) `alpha * ((fund_avg_price - avg_price) / avg_price) ** 2`
2) `beta * ((fund_alloc_ratio - desired_ratio) / desired_ratio) ** 2`

Where the first factor represents the squared normalized difference between the
average price for the fund and the average price for all trades and the
second factor represents the squared normalized difference between the allocated
ratio and the desired allocated ratio for the fund.

The parameters alpha and beta represent how representative each factor is to
the given problem.

The error (or result) is calculated for each solution as the sum of the 
factors for each fund.

--Serialization: For each asset, all the trades that must be inserted are broken 
down into the minimum traded amounts. Each of these represents a 
data block (gene). Each solution is represented as the collection of 
every data block with a single value representing to which fund that 
trade will be allocated.

--Base Solution: The base solution is defined as a solution that will be used as
a copy for initializing other solutions. It is a blank solution, where all 
trades have no allocated values.

--Random Solution: A random solution is a solution that copies the base 
solution, and each allocated value is random.

--Greedy Solution: A greedy solution is defined as a quick heuristics solution 
to speed up the process. In this case, a greedy solution is a solution that 
copies the base solution, and each allocated value has a probability of being
allocated that equals the desired ratio for each fund.

--Outputs: Once processed, the best allocation solution (minimal error) 
is exported to a file on the same folder with a 
name "Exports_%Y%m%d%H%M%S.xlsx".